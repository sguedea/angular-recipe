import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Recipe } from 'src/app/models/recipe.model';
import { RecipeService } from 'src/app/services/recipe.service';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit {
  // recipes: Recipe[] = [
  //   new Recipe("A Test Recipe", "This is a test", "https://imagesvc.meredithcorp.io/v3/mm/image?q=85&c=sc&poi=face&w=2000&h=1000&url=https%3A%2F%2Fstatic.onecms.io%2Fwp-content%2Fuploads%2Fsites%2F43%2F2020%2F07%2F22%2F8000900-2000.jpg" ),
  //   new Recipe("A Second Test Recipe", "This is another test", "https://i2.wp.com/www.downshiftology.com/wp-content/uploads/2018/12/Shakshuka-19.jpg" )
  // ];
  recipes: Recipe[]=[];

  @Output() recipeWasSelected = new EventEmitter<Recipe>();

  constructor(private recipeSvc: RecipeService) { }

  ngOnInit(): void {
    this.retrieveRecipes();
  }

  onRecipeSelected(recipe: Recipe) {
    this.recipeWasSelected.emit(recipe);
  }


  retrieveRecipes() {
    this.recipeSvc.getRecipes().subscribe(data => {
      this.recipes = data;
    })
  }
  

}
