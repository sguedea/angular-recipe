import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Recipe } from 'src/app/models/recipe.model';
import { RecipeService } from 'src/app/services/recipe.service';

@Component({
  selector: 'app-recipe-item',
  templateUrl: './recipe-item.component.html',
  styleUrls: ['./recipe-item.component.css']
})
export class RecipeItemComponent implements OnInit {
  @Input() recipe!: Recipe;
  // @Output() recipeSelected = new EventEmitter<void>();

  constructor(private recipeSvc: RecipeService) { }

  ngOnInit(): void {
  }

  onSelected() {
    // this.recipeSelected.emit();
    this.recipeSvc.recipeSelected.emit(this.recipe);
  }

}
