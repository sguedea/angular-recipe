export class Recipe {
    public name: string;
    public description: string;
    public imagePath: string;

    // initialize these properties in a constructor

    constructor(name: string, desc: string, imagePath: string) {
        this.name = name;
        this.description = desc;
        this.imagePath = imagePath;
    }

}